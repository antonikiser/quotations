class QuotationsController < ApplicationController
	def index
		@quotations = Quotation.all
	end
	
	
	def show
		@quotation = Quotation.find(params[:id])
	end

	
	def new
		@quotation = Quotation.new
	end
	
	
	def create 
		@quotation = Quotation.new(quotation_params)
		
		if @quotation.save

			redirect_to  action: "index"
		else
			render 'new'
		end	
	end
	
	
	private 
		def quotation_params
			params.require(:quotation).permit(:author, :text)
		end
end
