class CommentsController < ApplicationController
	def create
		@quotation = Quotation.find(params[:quotation_id])
		@comment = @quotation.comments.create(comment_params)
		redirect_to quotation_path(@quotation)
	
	end
 
	private
		def comment_params
		params.require(:comment).permit(:commenter, :body)
	end
end
