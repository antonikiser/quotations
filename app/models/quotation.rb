class Quotation < ApplicationRecord
	has_many :comments
	validates :author, presence: true,
					  length: { minimum: 1 }
	validates :text, presence: true,
					  length: { minimum: 1 }	  
end
