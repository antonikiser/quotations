class Comment < ApplicationRecord
  belongs_to :quotation
  validates :commenter, presence: true,
					length: { minimum: 1 }  
end
