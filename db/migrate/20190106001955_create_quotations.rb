class CreateQuotations < ActiveRecord::Migration[5.1]
  def change
    create_table :quotations do |t|
      t.string :author
      t.text :text

      t.timestamps
    end
  end
end
